# -*- coding: utf-8 -*-
from requests import Session, post, get
from requests.auth import HTTPBasicAuth
import json
import logging
from urllib.parse import urljoin

host = "https://ispyb.esrf.fr/ispyb/ispyb-ws/rest/"
user = "mx415"
password = "mx4154UO!"
beamline = "bm29"

# proposal = ("opd", "29")

session = Session()
session.auth = HTTPBasicAuth(user, password)
rest_token = ""

auth_url = urljoin(host, "authenticate?site=" + "ESRF")

try:
    data = {"login": str(user), "password": str(password)}
    response = post(auth_url, data=data)
    rest_token = response.json().get("token")
    print(rest_token)
except Exception as ex:
    msg = "POST to %s failed reason %s" % (auth_url, str(ex))
    logging.getLogger("ispyb_client").exception(msg)



# session_list = get(host + rest_token + "/proposal/" + user + "/session/list")
# print(session_list.json())

# DataCollectionRestWebService = get(host + rest_token + "/proposal/" + user + "/saxs/datacollection/list")
# print(DataCollectionRestWebService.json())

def list_experiments(rest_token, user):
    experiment_list = get(host + rest_token + "/proposal/" + user + "/saxs/experiment/list")
    print(experiment_list.json())


def createExperiment(rest_token, user):
    try:
        url = host + rest_token + "/proposal/" + user + "/saxs/experiment/save"
        data = {
            "name": "Test_rest_w2",
            "comments": "This is a test comment",
            "measurements": """[
                {"SEUtemperature":"","buffer":"B-Test","buffername":"B-Test","code":"B-Test","comments":"","concentration":0,"enable":True,"flow":True,"recuperate":False,"title":"","transmission":100,"macromolecule":"B-Test","type":"Buffer","typen":1,"viscosity":"low","volume":160,"volumeToLoad":40,"waittime":0,"plate":2,"row":1,"well":9},
                {"SEUtemperature":4,"buffer":"B-Test","buffername":"B-Test","code":"M-Test","comments":"","concentration":1,"enable":True,"flow":True,"recuperate":False,"title":"","transmission":100,"macromolecule":"M-Test","type":"Sample","typen":1,"viscosity":"low","volume":40,"volumeToLoad":40,"waittime":0,"plate":2,"row":1,"well":1},
                {"SEUtemperature":4,"buffer":"B-Test","buffername":"B-Test","code":"M-Test","comments":"","concentration":2,"enable":True,"flow":True,"recuperate":False,"title":"","transmission":100,"macromolecule":"M-Test","type":"Sample","typen":1,"viscosity":"low","volume":40,"volumeToLoad":40,"waittime":0,"plate":2,"row":1,"well":2}
            ]"""
        }
        response = post(url, data)
        print(response.text)
    except Exception as ex:
        msg = "POST to %s failed reason %s" % (url, str(ex))
        logging.getLogger("ispyb_client").exception(msg)


createExperiment(rest_token, user)
list_experiments(rest_token, user)